
Element["" "" "SW000" "" 200.00mil 300.00mil -150.00mil 210.00mil 0 100 ""]
(
	Pin[-100.00mil 160.00mil 90.00mil 0.7000mm 96.00mil 60.00mil "3" "3" ""]
	Pin[0.0000 160.00mil 90.00mil 0.7000mm 96.00mil 60.00mil "2" "2" ""]
	Pin[100.00mil 160.00mil 90.00mil 0.7000mm 96.00mil 60.00mil "1" "1" "square"]
	Pin[100.00mil -40.00mil 90.00mil 0.7000mm 96.00mil 60.00mil "4" "4" ""]
	Pin[-100.00mil -40.00mil 90.00mil 0.7000mm 96.00mil 60.00mil "4" "4" ""]
	ElementLine [-160.00mil 175.00mil -150.00mil 175.00mil 0.2000mm]
	ElementLine [160.00mil 175.00mil 150.00mil 175.00mil 0.2000mm]
	ElementLine [160.00mil -200.00mil 160.00mil 175.00mil 0.2000mm]
	ElementLine [-160.00mil -200.00mil 160.00mil -200.00mil 0.2000mm]
	ElementLine [-160.00mil 175.00mil -160.00mil -200.00mil 0.2000mm]

	)
